﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SGLR2.Hubs
{
    public class NotificacionHub : Hub
    {
        public async Task SendNotificacion(string room,string user,string message)
        {
            await Clients.Others.SendAsync("ReceiveNotificacion", room,user,message); //Envía la notificación a todos los clientes conectados
        }
    }
}
