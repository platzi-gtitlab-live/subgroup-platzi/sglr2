﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SGLR2.Hubs
{
    public class PositionHub : Hub
    {
        public async Task SendPosition(int left, int top)
        {
            await Clients.Others.SendAsync("ReceivePosition", left,top); //Envía la notificación a todos los clientes conectados
        }
    }
}
