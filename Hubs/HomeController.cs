﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SGLR2.Hubs
{
    public class HomeController : Controller
    {
        private readonly IHubContext<PositionHub> _notificationHub;

        public HomeController(IHubContext<PositionHub> notificationHub)
        {
            _notificationHub = notificationHub;
        }

        public async Task<IActionResult> Index()
        {
            await _notificationHub.Clients.All.SendAsync("ReceiveNotification", "Nueva notificación recibida");
            return Ok();
        }
        }
    }
