﻿import * as signalR from "@microsoft/signalr";

const connection = new signalR.HubConnectionBuilder()
    .withUrl("/notificationHub")
    .build();

connection.on("ReceiveNotification", (message) => {
    alert(message);
});

connection.start()
    .then(() => console.log("Conectado al hub de notificaciones"))
    .catch(err => console.log(err));